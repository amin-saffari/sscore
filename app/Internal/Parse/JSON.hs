{-# LANGUAGE OverloadedStrings #-}

module Internal.Parse.JSON where

import Data.Aeson 
import Types
-- Paser import
import Data.Word
import Data.Attoparsec.ByteString.Char8
import Control.Applicative

import Internal.GTF

-- JSON generic function
instance FromJSON GTFrecord
instance ToJSON GTFrecord

-------------------------------------------------------------------------------
instance FromJSON Attribute
instance ToJSON Attribute

-------------------------------------------------------------------------------

