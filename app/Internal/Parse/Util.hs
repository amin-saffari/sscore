{-# LANGUAGE OverloadedStrings #-}

module Internal.Parse.Util (tab,colon) where

import Data.Attoparsec.ByteString.Char8
import Internal.Types

tab :: Parser Char
tab = char '\t'

colon :: Parser Char
colon = char ':'

